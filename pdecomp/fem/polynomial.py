import itertools
import numpy as np

#############################################################################################################

class Monomial:
    VARNAMES = ['x', 'y', 'z']

    def __init__(self, lst):
        if isinstance(lst, dict):
            assert all( isinstance(k, int) and isinstance(v, int) and v>=0 and k>=0 for k,v in lst.items() ), lst
            self._variables = {k:v for k,v in lst.items() if v>0}

        elif isinstance(lst, list):
            variables = {}
            for l in lst:
                if isinstance(l, int):
                    assert l>=0
                    if l>0:
                        variables[l] = variables.get(l, 0) + 1
                elif isinstance(l, Monomial):
                    for e, n in l._variables.items():
                        if n>0:
                            variables[e] = variables.get(e, 0) + n
                else:
                    raise Exception(f"Unsupported class {l.__class__.__name__}")
            self._variables = variables
        
        else:
            raise ValueError(f"Unsupported argument type {lst.__class__}")
        
        self._hash = hash( frozenset( self._variables.items() ) )

    @property
    def degree(self):
        return sum( self._variables.values() )

    def is_unit(self):
        return self.degree==0

    def _varname(self, var):
        return self.VARNAMES[var] if 0<=var<len(self.VARNAMES) else var

    def __repr__(self):
        res = []
        for e in sorted(self._variables.keys()):
            n = self._variables[e]
            res.append( f"{self._varname(e)}" if n==1 else f"{self._varname(e)}^{n}" )
        return " ".join(res) if len(res)>0 else "1"

    def __hash__(self):
        return self._hash

    def __eq__(self, other):
        return isinstance(other, Monomial) and self._variables == other._variables

    def d(self, var):
        degree = self._variables.get(var, 0)
        if degree<1:
            return Polynomial.constant(0)
        return Polynomial({Monomial({**self._variables, var:degree-1}): degree})

    def anti_d(self, var):
        degree = self._variables.get(var, 0)
        return Polynomial({Monomial({**self._variables, var:degree+1}): 1.0/(degree+1)})

    def as_polynomial(self):
        return Polynomial({self:1})

    def __add__(self, other):
        if isinstance(other, Polynomial) or isinstance(other, Monomial):
            return self.as_polynomial()+other.as_polynomial()
        else:
            return NotImplemented

    def __sub__(self, other):
        if isinstance(other, Polynomial) or isinstance(other, Monomial):
            return self.as_polynomial()-other.as_polynomial()
        else:
            return NotImplemented

    def __mul__(self, other):
        if isinstance(other, Monomial):
            return Monomial(lst=[self, other])
        elif isinstance(other, Polynomial):
            return self.as_polynomial()*other
        elif isinstance(other, int) or isinstance(other, float):
            return self.as_polynomial()*other
        else:
            return NotImplemented

    def __neg__(self):
        return self*(-1)
       
    def __pow__(self, degree):
        if isinstance(degree, int) and degree>=0: 
            return Monomial({k:degree*v for k,v in self._variables.items()})
        else:
            return NotImplemented

    def change_coordinates(self, new):
        assert isinstance(new, list)
        assert all([isinstance(k,Polynomial) for k in new])
        res = Polynomial.constant(1)
        for k,v in self._variables.items():
            res *= new[k]**v
        return res

#############################################################################################################

class Polynomial:
    """
    Linear combination of monomials. 
    """
    def __init__(self, summands):
        assert isinstance(summands, dict)
        assert all( isinstance(k,Monomial) for k,p in summands.items() )
        self._summands = {k:v for k,v in summands.items() if v!=0}
 
    def __eq__(self, other):
        if isinstance(other, Polynomial):
            return self._summands == other._summands
        else:
            return False 

    def to_float(self):
        if len(self._summands)==0:
            return 0
        constant = Monomial({})
        if len(self._summands)>1 or constant not in self._summands:
            raise ValueError(f"Can not convert {self} to float")
        return self._summands[constant]

    def as_polynomial(self):
        return self

    @property
    def len(self):
        return len(self._summands)

    @classmethod
    def constant(cls, n: float):
        return Polynomial({Monomial({}): n})

    @classmethod
    def variable(cls, var):
        assert isinstance(var, int)
        return Polynomial( {Monomial({var: 1}): 1})

    def _filter(self):
        self._summands = { k:v for k,v in self._summands.items() if v!=0 }

    def append(self, coefficient: float, product: Monomial):
        p = self._summands.get(product, 0)
        self._summands[product] = p + coefficient
        self._filter()

    def __repr__(self):
        if self.is_zero(): return "0"
        r = [ f"{c}" if p.is_unit() else f"{p}" if c==1. else f"-{p}" if c==-1. else f"{c} {p}" for p,c in self._summands.items() ]
        s = [ r[0] ] + [ f" - {v[1:]}" if v[0]=='-' else f" + {v}" for v in r[1:] ]
        return "".join(s)

    def is_zero(self):
        return len(self._summands)==0

    def is_unit(self):
        return self._summands == { Monomial({}): 1.0 }

    def is_negative_unit(self):
        return self._summands == { Monomial({}): -1.0 }

    def __add__(self, other):
        if isinstance(other, Polynomial):
            r = Polynomial(summands=self._summands)
            for p, c in other._summands.items():
                r.append(c, p)
            return r
        else:
            return NotImplemented

    def __sub__(self, other):
        if isinstance(other, Polynomial):
            r = Polynomial(summands=self._summands)
            for p, c in other._summands.items():
                r.append(-c, p)
            return r
        else:
            return NotImplemented

    def __mul__(self, other):
        if isinstance(other, Polynomial):
            r = Polynomial(summands={})
            for p1, c1 in self._summands.items():
                for p2, c2 in other._summands.items():
                    r.append(c1*c2, p1*p2)
            return r
        elif isinstance(other, float) or isinstance(other, int):
            r = Polynomial(summands={})
            for p1, c1 in self._summands.items():
                    r.append(c1*other, p1)
            return r
        else:
            return NotImplemented

    def __rmul__(self, other):
        if isinstance(other, int) or isinstance(other, float):
            return self*other
        else:
            return NotImplemented

    def __neg__(self):
        return self*(-1)

    def d(self, var):
        result = Polynomial({})
        for p, c in self._summands.items():
            result = result + p.d(var)*c
        return result

    def anti_d(self, var):
        result = Polynomial({})
        for p, c in self._summands.items():
            result = result + p.anti_d(var)*c
        return result

    def _integrate_var(self, var, dim=3):
        v = self.anti_d(var)
        r = Polynomial.constant(1)
        for n in range(var):
            r = r - Polynomial.variable(n)
        subst = [Polynomial.variable(k) if k!=var else r for k in range(dim)]
        return v.change_coordinates(subst)

    def __pow__(self, degree):
        if isinstance(degree, int) and degree>=0: 
            msk = [a=='1' for a in reversed(f"{degree:b}")]
            powers = [self]
            for _ in range(1, len(msk)):
                powers.append(powers[-1]*powers[-1])
            res = Polynomial.constant(1)
            for m,f in zip(msk,powers):
                if m:
                    res = res * f
            return res
        else:
            return NotImplemented

    def change_coordinates(self, new):
        res = Polynomial.constant(0)
        for s, c in self._summands.items():
            res = res + s.change_coordinates(new)*c
        return res

    # @classmethod
    # def linear_element(cls, inv_matrix:np.ndarray, support:int):
    #     a = Polynomial.variable(support)
    #     subst = linear_change_of_coordinates(inv_matrix)
    #     return a.change_coordinates(subst)

    def integrate_over_unit_simplex(self, dim):
        res = self
        for var in range(dim-1, -1, -1):
            res = res._integrate_var(var, dim=dim)
        return res.to_float()

    def integrate_over_simplex(self, list_of_vertices):
        dim = len(list_of_vertices)-1
        matrix = simplex_vertices_to_matrix(list_of_vertices)
        # inv_matrix = np.linalg.inv(matrix)
        substitution = linear_change_of_coordinates(matrix)
        poly = self.change_coordinates(substitution)
        integral = poly.integrate_over_unit_simplex(dim=dim)
        vol = np.linalg.det(matrix[:dim,:dim])
        return integral*vol

    def integrate_over_cube(self, a=1):
        result = 0
        for simplex in cube_to_simplices(a=a):
            result += self.integrate_over_simplex(simplex)
        return result     

    def evaluate(self, point):
        substitution = list(map(Polynomial.constant, point))
        return self.change_coordinates(substitution).to_float()


###############################################################################################################################

def simplex_vertices_to_matrix(list_of_vertices):
    list_of_vertices = np.array(list_of_vertices).T
    dim = list_of_vertices.shape[0]
    assert list_of_vertices.shape==(dim,dim+1)
    matrix = np.eye(dim+1, dtype=np.float64)
    origin = list_of_vertices[:,0]
    matrix[:dim,-1] = origin
    matrix[:dim,:dim] = list_of_vertices[:,1:]-origin[:,None]
    return matrix

def linear_change_of_coordinates(matrix):
    substitutions = []
    for row in matrix[:-1]:
        poly = Polynomial.constant(row[-1])
        for n,c in enumerate(row[:-1]):
            poly = poly + Polynomial.variable(n)*c
        substitutions.append(poly)
    return substitutions


###############################################################################################################################

def cube_to_simplices(a=1, dim=3):
    if dim == 3:
        return [
            [(0,0,0),(a,0,0),(0,a,0),(0,0,a)],
            [(a,a,0),(a,0,0),(a,a,a),(0,a,0)],
            [(a,0,a),(a,0,0),(0,0,a),(a,a,a)],
            [(0,a,a),(0,0,a),(0,a,0),(a,a,a)],
            [(a,0,0),(0,a,0),(0,0,a),(a,a,a)],
        ]
    elif dim == 2:
        return [
            [(0,0),(a,0),(0,a)],
            [(a,a),(0,a),(a,0)],
        ]
    elif dim == 1:
        return [
            [(0,),(a,)],
        ]
    elif dim == 0:
        return []
    else:
        raise NotImplementedError
