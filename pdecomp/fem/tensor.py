import itertools

from .expression import VariablesCombination
from .element import ElementsCombination, ElementsProduct, LinearElement
from .polynomial import cube_to_simplices

##############################################################################################################

class Tensor:
    @property
    def ZERO(self): return 0

    def __init__(self, shape):
        self._shape = tuple(shape)
        self._data = {}

    @property
    def shape(self):
        return self._shape

    def empty_like(self, shape=None):
        raise NotImplementedError

    @property
    def len(self):
        return {k:v.len for k,v in self._data.items()}

    def _fix_index(self, key):
        if isinstance(key, int):
            key = (key,)
        elif isinstance(key, tuple):
            assert all([isinstance(k, int) for k in key]), "Wrong index type"
        else:
            raise ValueError(f"Unsupported key {key}")
        assert len(key)==len(self._shape), "Wrong key length"
        assert all([0<=k<b for k,b in zip(key,self._shape)]), "Index out of bound"
        return key

    def __setitem__(self, key, value):
        if value == self.ZERO: return
        self._data[self._fix_index(key)] = value

    def __getitem__(self, key):
        return self._data.get(self._fix_index(key), self.ZERO)

    def __eq__(self, other):
        if not isinstance(other, self.__class__): return False
        if self._shape != other._shape: return False
        for k in self.all_indices():
            if self[k]!=other[k]: 
                return False
        return True

    def __repr__(self):
        return self._repr_slice()

    def _repr_slice(self, prefix=()):
        axis = len(prefix)
        if axis>=len(self._shape): return f"{self[prefix]}"
        size = self._shape[axis]
        res = []
        for n in range(size):
            res.append( self._repr_slice(prefix = prefix+(n,)) )
        return f"[ {', '.join(res)} ]"

    def all_indices(self):
        return itertools.product( *(tuple(range(n)) for n in self._shape) )

    def cross(self, other, axis=None):
        if axis is None:
            assert len(self._shape)==1 
            axis=0

        assert 0<=axis<len(self._shape)
        assert self._shape[axis]==3
        assert other._shape == self._shape

        def change(idx, n):
            return idx[:axis]+(n,)+idx[axis+1:]

        r = self.empty_like()
        for idx in self.all_indices():
            n = idx[axis]
            ia = change(idx, (n+1)%3)
            ib = change(idx, (n-1)%3)

            r[idx] = r[idx] + self[ia]*other[ib]
            r[idx] = r[idx] - self[ib]*other[ia]
        return r

    def diff(self, var):
        r = self.empty_like()
        for idx in self.all_indices():
            r[idx] = self._data[idx].diff(var)
        return r

    def convolve(self, other):
        assert self._shape == other._shape
        s = None
        for idx in self.all_indices():
            p = self[idx]*other[idx]
            s = p if s is None else s+p

        r = self.empty_like(shape=())
        if s is not None:
            r[()] = s
        return r

    def __mul__(self, other):
        if isinstance(other, Tensor):
            assert self._shape == other._shape
            return self.convolve(other)
        elif isinstance(other, int) or isinstance(other, float):
            r = self.empty_like()
            for k in self.all_indices():
                r[k] = self[k]*other            
            return r
        else:
            return NotImplemented

    def __pow__(self, other):
        if isinstance(other, int):
            if other == 2:
                return self*self
            raise NotImplementedError(f"Only squares are supported.")
        else:
            return NotImplemented

    def __add__(self, other):
        if isinstance(other, Tensor):
            assert other._shape == self._shape
            r = self.empty_like()
            for k in self.all_indices():
                r[k] = self[k]+other[k]            
            return r
        else:
            return NotImplemented

    def __sub__(self, other):
        if isinstance(other, Tensor):
            assert other._shape == self._shape
            r = self.empty_like()
            for k in self.all_indices():
                r[k] = self[k]-other[k]            
            return r
        else:
            return NotImplemented

    def to_c_code(self, result_name='result', **kwargs):
        result = []
        for k,v in self._data.items():
            idx = ','.join(k)
            if len(idx)>0: idx = f"[{idx}]"
            expr = v.to_c_code(**kwargs)
            cmd = f"{result_name}{idx} = {expr};"
            result.append( cmd )
        return '\n'.join(result)

    @property
    def variables(self):
        vars = {}
        for data in self._data.values():
            for v, d in data.variables.items():
                vars[v] = max(d, vars.get(v, 0))
        return vars


class VariablesTensor(Tensor):
    @property
    def ZERO(self): return VariablesCombination({})

    def empty_like(self, shape=None):
        return VariablesTensor(shape=self._shape if shape is None else shape)

    def brick_up(self):
        r = self.empty_like()
        for idx in self.all_indices():
            r[idx] = self._data[idx].brick_up()
        return r


class ElementsTensor(Tensor):
    @property
    def ZERO(self): return ElementsCombination({})

    def empty_like(self, shape=None):
        return ElementsTensor(shape=self._shape if shape is None else shape)

    def integrate_over_simplex(self, simplex):
        result = VariablesTensor(shape=self._shape)
        for idx in self.all_indices():
            result[idx] = self[idx].integrate_over_simplex(simplex)
        return result

    def integrate_over_cube(self, dim=3):
        result = VariablesTensor(shape=self._shape)
        for simplex in cube_to_simplices(dim=dim):
            result = result + self.integrate_over_simplex(simplex)
        return result


    def grad(self, dim):
        r = ElementsTensor(shape=self._shape+(dim,))
        for n in range(dim):
            for idx, val in self._data.items():
                r[idx+(n,)] = val.d(n)
        return r

    def div(self, axis=None):
        if axis is None:
            assert len(self._shape)==1 
            axis=0
        assert 0<=axis<len(self._shape)

        def remove(idx):
            return idx[:axis]+idx[axis+1:]

        r = ElementsTensor(shape=remove(self._shape))
        for idx, val in self._data.items():
            n = idx[axis]
            i = remove(idx)
            r[i] = r[i] + val.d(n)
        return r

    def rot(self, axis=None):
        if axis is None:
            assert len(self._shape)==1 
            axis=0
        assert 0<=axis<len(self._shape)
        assert self._shape[axis]==3

        def change(idx, n):
            return idx[:axis]+(n,)+idx[axis+1:]

        r = ElementsTensor(shape=self._shape)
        for idx, val in self._data.items():
            n = idx[axis]

            dst = change(idx, (n+1)%3)
            var = (n-1)%3
            r[dst] = r[dst] + val.d(var)

            dst = change(idx, (n-1)%3)
            var = (n+1)%3
            r[dst] = r[dst] - val.d(var)
        return r




##############################################################################################################

def tensor(name:str, dim: int=3, shape=()):
    r = ElementsTensor(shape=shape)
    for idx in r.all_indices():
        summands = {}
        for vertex in range(dim+1):
            product = ElementsProduct( {LinearElement(support=vertex): 1} ) 
            summands[product] = VariablesCombination.variable(var=name, index=idx, vertex=vertex)
        r[idx] = ElementsCombination( summands )
    return r




