import itertools
import numpy as np
from dataclasses import dataclass
from typing import Dict, List


#############################################################################################################
# Numerical expression

@dataclass
class AxesOrder:
    vertex: List[int]
    index: List[int]

    def max(self):
        return max(*self.vertex, *self.index)

    def len(self):
        return len(self.vertex)+len(self.index)

    def format(self, vertex, index):
        assert self.len()==self.max()+1
        result = [None]*self.len()
        for n,v in zip(self.vertex, vertex):
            result[n] = v
        for n,v in zip(self.index, index):
            result[n] = v
        return result
    
##############################################################

class Variable:
    def __init__(self, var: str, vertex, index=()):
        assert isinstance(var, str)
        # assert isinstance(vertex, int)
        assert isinstance(index, tuple) 
        assert all(isinstance(n, int) for n in index), index
        self._var = str(var)
        if isinstance(vertex, int):
            self._vertex = int(vertex)
        elif hasattr(vertex, '__iter__'):
            self._vertex = tuple(vertex)
            assert all([isinstance(v, int) for v in self._vertex])
        else:
            raise ValueError(f"Unsupported type {type(vertex)} for argument vertex {vertex}")
        self._index = tuple(index)
    
    def translate(self, vector):
        assert isinstance(self._vertex, tuple)
        assert len(vector) == len(self._vertex)
        new_vertex = tuple(a+b for a,b in zip(self._vertex,vector))
        return Variable(var=self._var, index=self._index, vertex=new_vertex)

    @property
    def vertex(self):
        return self._vertex

    def change_vertex(self, new_vertex):
        return Variable(var=self._var, vertex=new_vertex, index=self._index)

    def __repr__(self):
        r = "" if len(self._index)<1 else f"[{','.join(str(s) for s in self._index)}]"
        return f"{self._var}{r}@{self._vertex}"

    def to_c_code(self, vertex_prefix=['i','j','k'], axes_order={}):
        assert hasattr(self._vertex, '__iter__') 
        assert hasattr(self._index, '__iter__') 
        vertex = []
        for n,v in enumerate(self._vertex):
            name = f"{vertex_prefix[n]}" 
            if v!=0: name += f"{v:+d}"
            vertex.append(name)
        index = map(repr, self._index)
        # Reorder axis if necessary
        if self._var in axes_order:
            fmt = axes_order[self._var]
            assert isinstance(fmt, AxesOrder)
            indices = fmt.format(index=index, vertex=vertex)
        else:
            indices = [*vertex, *index]
        # Final formatting
        indices = ','.join(indices)
        if len(indices)>0: indices = f"[{indices}]"
        return f"{self._var}{indices}"        

    def __hash__(self):
        return hash((self._var, self._vertex, self._index))

    def __eq__(self, other):
        return isinstance(other, Variable) and self._var == other._var and self._vertex == other._vertex and self._index == other._index

#############################################################################################################

class VariablesProduct:
    def __init__(self, lst):
        if isinstance(lst, dict):
            assert all( isinstance(k, Variable) and isinstance(v, int) for k,v in lst.items() ), lst
            variables = {**lst}

        elif isinstance(lst, list):
            variables = {}
            for l in lst:
                if isinstance(l, Variable):
                    variables[l] = variables.get(l, 0) + 1
                elif isinstance(l, VariablesProduct):
                    for e, n in l._variables.items():
                        variables[e] = variables.get(e, 0) + n
                else:
                    raise Exception(f"Unsupported class {l.__class__.__name__}")
        
        else:
            raise ValueError(f"Unsupported argument type {lst.__class__}")
        
        self._variables = { k:v for k,v in variables.items() if v!=0 }

        self._hash = hash( frozenset( self._variables.items() ) )

    def translate(self, vector):
        variables = {}
        for e, n in self._variables.items():
            variables[e.translate(vector)] = n
        return VariablesProduct(variables)


    def change_vertices(self, mapping):
        assert isinstance(mapping, dict)
        variables = {}
        for e, n in self._variables.items():
            variables[e.change_vertex(mapping[e.vertex])] = n
        return VariablesProduct(variables)

    def is_unit(self):
        return len(self._variables)==0

    def __mul__(self, other):
        if isinstance(other, VariablesProduct) or isinstance(other, Variable):
            return VariablesProduct(lst=[self, other])
        else:
            return NotImplemented

    def __repr__(self):
        res = [f"{e}" if n==1 else f"{e}^{n}" for e, n in self._variables.items() ]
        return " ".join(res) if len(res)>0 else "1"

    def to_c_code(self, power_symbol='**', **kwargs):
        res = []
        for e, n in self._variables.items():
            ee = e.to_c_code(**kwargs)
            res.append( ee if n==1 else f"{ee}{power_symbol}{n}" )
        return "*".join(res) if len(res)>0 else "1"

    def __hash__(self):
        return self._hash

    def __eq__(self, other):
        return isinstance(other, VariablesProduct) and self._variables == other._variables

    @property
    def variables(self):
        return self._variables

    def diff(self, var):
        deg = self._variables.get(var,0)
        if deg<1:
            return None
        product = VariablesProduct({**self._variables, var: deg-1})
        return VariablesCombination({product: deg})



#############################################################################################################

class VariablesCombination:
    """
    Linear combination of product of variables and constants. 
    """
    TOL = 1e-8

    def __init__(self, summands):
        assert isinstance(summands, dict)
        assert all( isinstance(k,VariablesProduct) for k,p in summands.items() )        
        self._summands = {}
        for k,v in summands.items():
            self.append(coefficient=v, product=k)

    @property
    def len(self):
        return len(self._summands)

    @classmethod
    def constant(cls, n: float):
        return VariablesCombination({VariablesProduct({}): n})

    @classmethod
    def variable(cls, *vargs, **kwargs):
        var = Variable(*vargs, **kwargs)
        return VariablesCombination( {VariablesProduct({var: 1}): 1})

    def translate(self, vector):
        r = VariablesCombination(summands={})
        for p, c in self._summands.items():
            r.append(c, p.translate(vector))
        return r        

    def brick_up(self):
        example = list(self.variables)[0].vertex
        assert hasattr(example, '__iter__'), "Expression is not integrated."
        dim = len(example)
        result = self
        for d in range(dim):
            vector = tuple(-1 if d==n else 0 for n in range(dim))
            result = result + result.translate(vector)
        return result

    def change_vertices(self, mapping):
        r = VariablesCombination(summands={})
        for p, c in self._summands.items():
            r.append(c, p.change_vertices(mapping))
        return r        

    def _filter(self):
        self._summands = { k:v for k,v in self._summands.items() if v!=0 }

    def append(self, coefficient: float, product: VariablesProduct):
        if coefficient==0: return
        p = self._summands.get(product, 0)
        self._summands[product] = p + coefficient
        c = self._summands[product]
        if np.abs(c)<self.TOL:
            self._summands.pop(product)

    def __repr__(self):
        if self.is_zero(): return "0"
        r = []
        for  p,c in self._summands.items():
            cc = f"{c:.3f}".rstrip('0').rstrip('.')
            r.append( f"{cc}" if p.is_unit() else f"{p}" if np.abs(c-1.)<self.TOL else f"-{p}" if np.abs(c+1.)<self.TOL else f"{cc} {p}" )
        s = [ r[0] ] + [ f" - {v[1:]}" if v[0]=='-' else f" + {v}" for v in r[1:] ]
        return "".join(s)

    def is_zero(self):
        return len(self._summands)==0

    def is_unit(self):
        return self._summands == { VariablesProduct({}): 1.0 }

    def is_negative_unit(self):
        return self._summands == { VariablesProduct({}): -1.0 }

    def __add__(self, other):
        if isinstance(other, VariablesCombination):
            r = VariablesCombination(summands=self._summands)
            for p, c in other._summands.items():
                r.append(c, p)
            return r
        else:
            return NotImplemented

    def __sub__(self, other):
        if isinstance(other, VariablesCombination):
            r = VariablesCombination(summands=self._summands)
            for p, c in other._summands.items():
                r.append(-c, p)
            return r
        else:
            return NotImplemented

    def __mul__(self, other):
        if isinstance(other, VariablesCombination):
            r = VariablesCombination(summands={})
            for p1, c1 in self._summands.items():
                for p2, c2 in other._summands.items():
                    r.append(c1*c2, p1*p2)
            return r
        elif isinstance(other, float) or isinstance(other, int):
            r = VariablesCombination(summands={})
            for p1, c1 in self._summands.items():
                    r.append(c1*other, p1)
            return r
        else:
            return NotImplemented

    def __neg__(self):
        return self*(-1)        

    def to_c_code(self, digits=7, **kwargs):
        if self.is_zero(): return "0"
        r = []
        for  p,c in self._summands.items():
            c = np.around(c, digits)
            cc = f"{c}".rstrip('0').rstrip('.')
            pp = p.to_c_code(**kwargs)
            if p.is_unit():
                r.append( cc )
            elif cc == '1':
                r.append( pp )
            elif cc == '-1':
                r.append( f"-{pp}" )
            else:
                r.append( f"{cc}*{pp}" )
        s = [ r[0] ] + [ f" - {v[1:]}" if v[0]=='-' else f" + {v}" for v in r[1:] ]
        return "".join(s)

    @property
    def variables(self):
        vars = {}
        for product in self._summands.keys():
            for v, d in product.variables.items():
                vars[v] = max(d, vars.get(v, 0))
        return vars

    def diff(self, var):
        assert isinstance(var, Variable)
        result = VariablesCombination({})
        for p, c in self._summands.items():
            dp = p.diff(var)
            if dp is not None:
                result = result + dp*c
        return result
