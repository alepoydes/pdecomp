from .expression import VariablesCombination
from .polynomial import *

#############################################################################################################
# Finite elements

class Element:
    pass

##############################################################################################################

class LinearElement(Element):
    def __init__(self, support, derivatives=None):
        """Linear finite element. Equals one at one vertex and and equals zero at all other vertices.
        Between vertices function is linearly inteprolated.
        Vertices are enumerated by intergers starting from zero. 
        For tetrahedron vertices are 0, 1, 2 and 3.

        Args:
            support (int): Vertex where element is equal to one.
        """
        self._support = support
        assert 0 <= self._support <= 3
        self._derivatives = {} if derivatives is None else dict(derivatives)

    NAMES = ['x','y','z']

    def __repr__(self):
        der = "".join([f"d{self.NAMES[v]}"+(f"^{o} " if o>1 else " ") for v,o in self._derivatives.items()])
        return f"{der}L@{self._support}"

    def __hash__(self):
        return self._support

    def __eq__(self, other):
        return isinstance(other, LinearElement) and self._support == other._support and self._derivatives == other._derivatives

    def dorder(self):
        return sum([n for _,n in self._derivatives.items()])

    def d(self, var):
        if self.dorder()>=1:
            return None
        der = self._derivatives.get(var, 0) + 1
        return LinearElement(support=self._support, derivatives={**self._derivatives, var: der})

    def to_polynomial(self, simplex):
        dim = len(simplex)-1
        if self._support>0:
            poly = Polynomial.variable(var=self._support-1)
        else:
            poly = Polynomial.constant(1)
            for d in range(dim):
                poly = poly - Polynomial.variable(var=d)
        matrix = simplex_vertices_to_matrix(simplex)
        inv_matrix = np.linalg.inv(matrix)
        substitution = linear_change_of_coordinates(inv_matrix)
        poly = poly.change_coordinates(substitution)
        for v,d in self._derivatives.items():
            for _ in range(d):
                poly = poly.d(v)
        return poly


##############################################################################################################

class ElementsProduct:
    def __init__(self, lst):
        if isinstance(lst, dict):
            assert all([ isinstance(k,Element) and isinstance(v,int) for k,v in lst.items() ])
            self._elements = {**lst}
        elif isinstance(lst, list):
            self._elements = {}
            for l in lst:
                if isinstance(l, Element):
                    self._elements[l] = self._elements.get(l, 0) + 1
                elif isinstance(l, ElementsProduct):
                    for e, n in l._elements.items():
                        assert n>=0
                        if n>0:
                            self._elements[e] = self._elements.get(e, 0) + n
                elif l is None:
                    pass
                else:
                    raise Exception(f"Unsupported class {l.__class__.__name__}")
        else:
            raise Exception(f"Unsupported class {lst.__class__.__name__}")

        self._hash = hash( frozenset( self._elements.items() ) )

    def is_unit(self):
        return len(self._elements)==0

    def __mul__(self, other):
        if isinstance(other, ElementsProduct) or isinstance(other, Element):
            return ElementsProduct(lst=[self, other])
        else:
            return NotImplemented

    def __repr__(self):
        res = [f"{e}" if n==1 else f"{e}^{n}" for e, n in self._elements.items() ]
        par = [ f"({r})" if r[0]=='d' else r for r in res ]
        return " ".join(par) if len(par)>0 else "1"

    def __hash__(self):
        return self._hash
        # return hash(self._elements)

    def __eq__(self, other):
        return isinstance(other, ElementsProduct) and self._elements == other._elements

    def d(self, var):
        summands = ElementsCombination({})
        for e, n in self._elements.items():
            diff = e.d(var)
            if diff is None: continue
            copy = self._elements.copy()
            copy.pop(e)
            product = ElementsProduct([ElementsProduct(copy), ElementsProduct({e:n-1}), diff])
            summands.append(coefficient=VariablesCombination.constant(n), product=product)
        return summands

    def to_polynomial(self, simplex):
        poly = Polynomial.constant(1)
        for e,n in self._elements.items():
            poly = poly * e.to_polynomial(simplex)**n
        return poly

    def integrate_over_simplex(self, simplex):
        poly = self.to_polynomial(simplex)
        return poly.integrate_over_simplex(simplex)

##############################################################################################################

class ElementsCombination:
    """
    Linear combination of product of finite elements. 
    Represents an approximation to the vector field with `dim` components over a simplex.
    """
    def __init__(self, summands):
        assert isinstance(summands, dict)
        self._summands = {}
        for k,v in summands.items():
            self.append(coefficient=v, product=k)

    def _filter(self):
        self._summands = { k:v for k,v in self._summands.items() if not v.is_zero() }

    def append(self, coefficient: VariablesCombination, product: ElementsProduct):
        if isinstance(coefficient, int) or isinstance(coefficient, float):
            coefficient = VariablesCombination.constant(coefficient)
        elif isinstance(coefficient, VariablesCombination):
            pass
        else:
            raise ValueError(f"Unsopported coefficient {coefficient}")
        p = self._summands.get(product, VariablesCombination({}))
        self._summands[product] = p + coefficient
        self._filter()

    def is_zero(self):
        return len(self._summands)==0

    def __eq__(self, other):
        if not isinstance(other, ElementsCombination):
            return False
        return self._summands == other._summands

    def _format(self, c: VariablesCombination):
        assert isinstance(c, VariablesCombination)
        if c.len == 1: 
            return repr(c)
        return f"({c})"

    def __repr__(self):
        if self.is_zero(): return "0"
        r = [ f"{c}" if p.is_unit() else f"{p}" if c.is_unit() else f"-{p}" if c.is_negative_unit() else f"{self._format(c)} {p}" for p,c in self._summands.items() ]
        s = [ r[0] ] + [ f" - {v[1:]}" if v[0]=='-' else f" + {v}" for v in r[1:] ]
        return "".join(s)


    def __add__(self, other):
        if isinstance(other, ElementsCombination):
            r = ElementsCombination(summands=self._summands)
            for p, c in other._summands.items():
                r.append(c, p)
            return r
        else:
            return NotImplemented

    def __sub__(self, other):
        if isinstance(other, ElementsCombination):
            r = ElementsCombination(summands=self._summands)
            for p, c in other._summands.items():
                r.append(-c, p)
            return r
        else:
            return NotImplemented

    def __mul__(self, other):
        if isinstance(other, ElementsCombination):
            r = ElementsCombination(summands={})
            for p1, c1 in self._summands.items():
                for p2, c2 in other._summands.items():
                    r.append(c1*c2, p1*p2)
            return r
        elif isinstance(other, VariablesCombination):
            r = ElementsCombination(summands={})
            for p1, c1 in self._summands.items():
                    r.append(c1*other, p1)
            return r
        else:
            return NotImplemented

    def d(self, var):
        result = ElementsCombination({})
        for p, c in self._summands.items():
            result = result + p.d(var)*c
        return result

    def integrate_over_simplex(self, simplex):
        result = VariablesCombination({})
        for p, c in self._summands.items():
            mapping = dict(enumerate(simplex))
            result = result + c.change_vertices(mapping)*p.integrate_over_simplex(simplex)
        return result

    def to_c_code(self, *vargs, **kwargs):
        raise NotImplementedError
