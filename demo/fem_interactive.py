# %%
import pdecomp.fem as fem

# %%
d = 1
n = fem.tensor(name='n', shape=(1,), dim=d)
f = n**2
s = f.integrate_over_cube(dim=d)
print(f"         f={f}\n")
print(f"integral 3f={s*3}")

# %%
e = f

# %%
e.d(0)

# %%
n = fem.tensor(name='n', shape=(3,), dim=3)
q = fem.tensor(name='q', shape=(), dim=3)
# print(n.grad(1)**2)
# print(n.rot()**2)
print("n", n, '\n')
print("(nabla n)^2 = ", n.grad(3)**2, '\n')
print("int (nabla n)^2 = ", (n.grad(3)**2).integrate_over_cube(3), '\n')
# print(n.div()**2, '\n')
# print( (n*n.rot()+q)**2 , '\n')
# print(n.cross(n.rot())**2, '\n')
# print(n.cross(q.grad(dim=3)), '\n')


# %%
c = fem.tensor(name='n', shape=(3,), dim=3)
s = fem.tensor(name='s', dim=3)
print(f"{c = }")
print(f"{c[1] = }")
print(f"{c.div() = }")
print(f"{c.rot() = }")
print(f"{s = }")
print(f"{s.grad(3) = }")
print(f"{c**2=}")



# %%
e1 = fem.LinearElement(support=0)
e2 = fem.LinearElement(support=1)


# %%
p1 = fem.ElementsProduct({e1:3, e2:2})
# p1 = fem.ElementsProduct([e1, e2, e2, e1, e1])
p2 = fem.ElementsProduct([e1, e1, e2])
p3 = fem.ElementsProduct([e1, e2])
print(p1)
print(p1.d(1))


# %%
c = fem.ElementsCombination({p1:fem.VariablesCombination.constant(1), p2:3, p3:-1})
print(c)

# %%
c = fem.ElementsCombination({p3:1})
print(c)
print(c.d(0))
print(c.d(0).d(1))
print(c.d(0).d(1).d(0))





# %%
c1 = fem.VariablesCombination.constant(3)
c2 = fem.VariablesCombination.variable(var='n', vertex=1)
(c1+c2)*(c1-c2)


# %%
n = fem.tensor(name='n', shape=(3,), dim=3)

# %%
v1 = fem.Variable(var='n', vertex=1, index=(3,2))
v2 = fem.Variable(var='n', vertex=0, index=(2,3))


# %%
p = fem.VariablesProduct([v1, v2])

# %%
