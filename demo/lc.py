# Liquid crystal energy

# %%
import pdecomp.fem as fem
from termcolor import colored

# %%
n = fem.tensor(name='n', shape=(3,), dim=3) # director vector field
q = fem.tensor(name='q', shape=(), dim=3) # wave number scalar field
axes_order ={
    'n': fem.AxesOrder(vertex=[3,2,1], index=[0]),
    'q': fem.AxesOrder(vertex=[2,1,0], index=[])
}
nv = [ fem.Variable(var='n', vertex=[0,0,0], index=(d,)) for d in range(3) ]
print(f"{nv = }")

def totlen(d):
    return sum(d.len.values())

def process(tensor, name='tmp', multiplier=1):
    # print(colored('tensor',"red"), tensor)
    integral = tensor.integrate_over_cube(dim=3) * multiplier
    with open(f"tmp/{name}.e.txt",'tw') as file:
        file.write(integral.to_c_code(axes_order=axes_order))
    # print(colored('integral',"red"), integral) 
    bricked = integral.brick_up()   
    # print(integral.to_c_code(axes_order=axes_order))
    print(colored("number of addenda",'red'), colored(f"{totlen(integral)} {totlen(bricked)}",'green'))
    # vars = list(integral.variables)
    # print(colored("variables",'red'), vars)
    # for v in vars:
    for dim, v in enumerate(nv):
        # d = integral.diff(v)
        d = bricked.diff(v)
        l = totlen(d)
        s = d.to_c_code(axes_order=axes_order)
        print(colored(f"d / d {v}","yellow"), colored(f'{l}','green'), len(s))
        with open(f"tmp/{name}.e{dim}.txt",'tw') as file:
            file.write(s)

# %%
t = n**2
process(t, multiplier=15)

# %% 
# K1/2/gamma/L^2 * ...
k1 = (n.grad(dim=3))**2
process(k1, multiplier=6, name='k1')

# %%
# (K3-K1)/2/gamma/L^2 * ...
k31 = (n.rot())**2 
process(k31, multiplier=6, name='k31')

# %%
# (K2-K3)/2/gamma/L^2 * ...
k23 = (n*n.rot())**2
process(k23, name='k23', multiplier=120)

# %%
# 2*K2*L/2/gamma/L^2 * ...
k2q = (n*n.rot())*q
process(k2q, multiplier=120, name='k2q')


# %%
# K2*L^2/2/gamma/L^2 * ...
kq2 = q**2
process(kq2, name='kq2')


# %%

# %%

# %%
