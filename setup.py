from setuptools import setup
import unittest
from itertools import chain

extras_require = {
}

extras_require['all'] = list(set(chain(*map(extras_require.__getitem__, []))))

setup(name='pdecomp',
      version='0.0.1',
      description='Partial differential expression compiler',
      url='https://gitlab.com/alepoydes/pdecomp.git',
      author='Igor Lobanov',
      author_email='lobanov.igor@gmail.com',
      license='GNU GPLv3',
      packages=['pdecomp'],
      install_requires=['pytest','numpy','numba'],
      # setup_requires=['pytest-runner'],
      # tests_require=['pytest'],
      extras_require=extras_require,
      scripts=[],
      include_package_data=True,
      test_suite='tests',
      zip_safe=False
      )