import pdecomp.fem as fem

import  numpy.testing as npt

def test_monomial():
    x = fem.Monomial({0:1})
    y = fem.Monomial({1:1})
    z = fem.Monomial({2:1})
    unit = fem.Monomial({})

    assert repr(unit) == '1'    
    assert repr(x) == 'x'
    assert repr(y) == 'y'
    assert repr(z) == 'z'
    assert repr(x*x) == 'x^2'
    assert repr(y*y) == 'y^2'
    assert repr(z*z) == 'z^2'
    assert repr(x*y) == 'x y'
    assert repr(x*z) == 'x z'
    assert repr(y*z) == 'y z'
    assert repr(z*y) == 'y z'
    assert repr(z*y*x) == 'x y z'
    assert repr(z*y*y) == 'y^2 z'

    assert x==x
    assert y==y
    assert x!=y
    assert x*x!=x

    assert x*y == y*x

    assert unit.degree == 0
    assert x.degree == 1
    assert (x*x).degree == 2
    assert (x*y).degree == 2
    assert (x*y*y).degree == 3
    assert (x*y*z).degree == 3

    assert unit.is_unit()
    assert not x.is_unit()
    assert not y.is_unit()
    assert not z.is_unit()
    assert not (x*z).is_unit()

    assert x**2 == x*x
    assert (x*y*y)**3 == x**3 * y**6


def test_arithemtics():
    x = fem.Polynomial.variable(0)
    y = fem.Polynomial.variable(1)
    z = fem.Polynomial.variable(2)
    unit = fem.Polynomial.constant(1)
    zero = fem.Polynomial.constant(0)

    assert x+zero == x
    assert x+y == y+x
    assert x*y == y*x
    assert -x*y == x*(-y)
    assert x**2 == x*x
    assert (x+y)*(x+y) == x*x + 2*x*y + y*y 
    assert (x-y)**2 == x**2 - 2*x*y + y**2
    assert (x-y)**3 == x**3 - 3*x**2*y + 3*x*y**2 - y**3
    assert (x+unit)**2 == x**2 + 2*x + unit

    assert x.evaluate([2]) == 2
    assert x.evaluate([2,3]) == 2
    assert (x*y+z).evaluate([3,2,1]) == 7



def test_diff():
    x = fem.Polynomial.variable(0)
    y = fem.Polynomial.variable(1)
    z = fem.Polynomial.variable(2)
    unit = fem.Polynomial.constant(1)
    zero = fem.Polynomial.constant(0)
    assert x.d(0) == unit
    assert x.d(1) == zero
    assert x.d(0).d(0) == zero
    assert (x**2).d(1) == zero
    assert (x**2).d(0) == 2*x
    assert (x**2).d(0).d(0) == 2*unit
    assert (x*y).d(0) == y
    assert (x*y).d(1) == x
    assert (x+y).d(0) == unit
    assert (x+y).d(1) == unit
    assert (x+y).d(2) == zero



def test_integral():
    x = fem.Polynomial.variable(0)
    y = fem.Polynomial.variable(1)
    z = fem.Polynomial.variable(2)
    unit = fem.Polynomial.constant(1)

    assert (unit).integrate_over_simplex([(0,0,0),(2,0,0),(0,3,0),(0,0,1)]) == 1.0
    assert (unit).integrate_over_simplex([(0,0,0),(1,0,0),(0,1,0),(0,0,1)]) == 1/6
    assert (unit).integrate_over_cube(a=1) == 1.
    npt.assert_almost_equal( (unit).integrate_over_cube(a=2), 8)
    npt.assert_almost_equal( (x).integrate_over_cube(a=1), 0.5)
    npt.assert_almost_equal( (y).integrate_over_cube(a=1), 0.5)
    npt.assert_almost_equal( (z).integrate_over_cube(a=1), 0.5)
    npt.assert_almost_equal( (x*x).integrate_over_cube(a=1), 1/3)
    npt.assert_almost_equal( (y*y).integrate_over_cube(a=1), 1/3)
    npt.assert_almost_equal( (z*z).integrate_over_cube(a=1), 1/3)
    npt.assert_almost_equal( (x*y).integrate_over_cube(a=1), 1/4)
    npt.assert_almost_equal( (y*z).integrate_over_cube(a=1), 1/4)
    npt.assert_almost_equal( (z*x).integrate_over_cube(a=1), 1/4)








