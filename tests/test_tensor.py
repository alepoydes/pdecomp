import pdecomp.fem as fem

def test_vector_calculus():
    n = fem.tensor(name='n', shape=(3,), dim=0)
    q = fem.tensor(name='q', shape=(), dim=0)
    # assert n.cross(n) == fem.ElementsTensor(shape=(3,))

def test_indices():
    t = fem.Tensor(shape=(2,3))
    assert list( t.all_indices() ) == [(0, 0), (0, 1), (0, 2), (1, 0), (1, 1), (1, 2)]