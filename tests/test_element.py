import pdecomp.fem as fem

def test_variables():
    v = fem.Variable(var='n', vertex=1, index=(2,3))
    assert repr(v) == 'n[2,3]@1'
    assert v == v
    assert v == fem.Variable(var='n', vertex=1, index=(2,3))
    assert v != fem.Variable(var='n1', vertex=1, index=(2,3))
    assert v != fem.Variable(var='m', vertex=1, index=(2,3))
    assert v != fem.Variable(var='n', vertex=0, index=(2,3))
    assert v != fem.Variable(var='n', vertex=0, index=())

def test_variables_product():
    v1 = fem.Variable(var='n', vertex=1, index=(2,3))
    v2 = fem.Variable(var='n', vertex=0, index=(2,3))
    v3 = fem.Variable(var='m', vertex=0, index=(2,3))

    assert fem.VariablesProduct([v1, v2]) == fem.VariablesProduct([v1, v2])
    assert fem.VariablesProduct([v1, v2]) == fem.VariablesProduct([v2, v1])
    assert fem.VariablesProduct([v1, v2, v3]) == fem.VariablesProduct([v2, v3, v1])
    assert fem.VariablesProduct([v1, v1, v3]) == fem.VariablesProduct([v1, v3, v1])

    assert fem.VariablesProduct([v1, v2, v3]) != fem.VariablesProduct([v1, v1, v3])
    assert fem.VariablesProduct([v1, v2, v3]) != fem.VariablesProduct([v1, v2])
    assert fem.VariablesProduct([v1, v2]) != fem.VariablesProduct([v1])
    assert fem.VariablesProduct([v1]) != fem.VariablesProduct([])

def test_element_to_polynomial():
    for simplex in fem.cube_to_simplices(a=2):
        for support in range(4):
            e = fem.LinearElement(support=support)
            poly = e.to_polynomial(simplex)
            for n, point in enumerate(simplex):
                v = poly.evaluate(point)
                assert v == (1.0 if n==support else 0.0)
                    
def test_element_derivative():
    simplex = fem.cube_to_simplices()[0]

    e = fem.LinearElement(support=0)
    for n in range(3):
        assert e.d(n).to_polynomial(simplex) == fem.Polynomial.constant(-1)

    for support in range(1,4):
        e = fem.LinearElement(support=support)
        for n in range(3):
            assert e.d(n).to_polynomial(simplex) == fem.Polynomial.constant(1 if support-1==n else 0)

        
